﻿using System;

namespace ToDoApp.Web.Models
{
    public class NoteModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public bool isDone { get; set; }

        public DateTime DueDate { get; set; }
    }
}