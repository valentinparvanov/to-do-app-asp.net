﻿namespace ToDoApp.Web.Controllers
{
    using Configuration;
    using Data.Entities;
    using Data.Repositories;
    using Models;
    using System.Web.Mvc;

    public class NotesController : Controller
    {
        private NoteRepository repository = new NoteRepository(AppConfig.ConnectionString);

        [HttpGet]
        public ActionResult Index()
        {
            var model = this.repository.GetAll();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(NoteModel model)
        {
            var note = new Note()
            {
                Title = model.Title,
                Description = model.Description,
                IsDone = model.isDone,
                DueDate = model.DueDate
            };

            this.repository.Insert(note);

            return Redirect("/Notes");
        }
    }
}