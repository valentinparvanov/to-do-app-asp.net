CREATE DATABASE TodoApp

USE TodoApp

CREATE TABLE Notes
(
Id INT IDENTITY(1,1),
Title NVARCHAR(MAX) NOT NULL,
[Description] NVARCHAR(MAX),
IsDone BIT,
DueDate DATETIME
CONSTRAINT PK_Notes_Id PRIMARY KEY (Id)
)

INSERT INTO Notes (Title, [Description], IsDone, DueDate)
VALUES ('Do my Hw', 'Write repository', 0, '2017-10-11')
