﻿namespace ToDoApp.Web.Configuration
{
    using System.Configuration;

    public static class AppConfig
    {
        static AppConfig()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["ToDoAppDb"].ToString();
        }

        public static string ConnectionString { get; set; }
    }
}