﻿namespace ToDoApp.Data.Repositories
{
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class NoteRepository
    {
        private string connectionString;

        public NoteRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Note> GetAll()
        {
            var result = new List<Note>();
            var connection = new SqlConnection(connectionString);

            using (connection)
            {
                connection.Open();

                var command = connection.CreateCommand();
                command.CommandText =
@"SELECT * FROM Notes";

                var reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        var note = new Note();
                        note.Id = (int)reader["Id"];
                        note.Title = (string)reader["Title"];
                        note.Description = (string)reader["Description"];
                        note.IsDone = (bool)reader["IsDone"];
                        note.DueDate = (DateTime)reader["DueDate"];

                        result.Add(note);
                    }
                }
            }
            return result;
        }

        public void Insert(Note note)
        {
            var connettion = new SqlConnection(connectionString);

            using (connettion)
            {
                connettion.Open();

                var command = connettion.CreateCommand();
                command.CommandText =
@"INSERT INTO Notes (Title, Description, IsDone, DueDate)
VALUES (@Title, @Description, @IsDone, @DueDate)";

                command.Parameters.AddWithValue("@Title", note.Title);
                command.Parameters.AddWithValue("@Description", note.Description);
                command.Parameters.AddWithValue("@IsDone", note.IsDone);
                command.Parameters.AddWithValue("@DueDate", note.IsDone);

                command.ExecuteNonQuery();
            }
        }
    }
}